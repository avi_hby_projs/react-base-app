import React from 'react';

const SideBar = (props) => {
    return (
        <>
            {props.appStructData.map((item) => <a href="#" key={item[0]} onClick={() => props.eventCall(item)} >{item[1]}</a>)}
        </>
    );
}

export default SideBar;
