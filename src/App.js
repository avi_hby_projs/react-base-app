import './App.css';
import SideBar from './SideBar';
import App1 from './App1';
import App2 from './App2';
import App3 from './App3';
import App4 from './App4';
import App5 from './App5';
import { useState } from 'react';

const appStruct = [
  ['1', 'About', [<App1 key='1' />]],
  ['2', 'Services', [<App2 key='2' />]],
  ['3', 'Clients', [<App3 key='3' />]],
  ['4', 'Contact', [<App4 key='4' />]],
  ['5', 'Bavri', [<App5 key='5' />]]
];

const KEY = 0;
const DISPLAY_NAME = 1;

function App() {

  const [appNo, setAppNo] = useState(['1', 'About', 'APP_1']);

  const handleSideBarEvent = (event) => {

    console.log(appNo + ' -> ' + event);
    setAppNo(event);
  };

  return (
    <>
      <div className="sidenav">
        <SideBar eventCall={handleSideBarEvent} appStructData={appStruct} />
      </div>
      <div className="main">{appStruct[appNo[0] - 1][2]}</div>
    </>
  );
}

export default App;
